package it.sisal.springrest.bean;

import java.util.Date;

public class Match {
	private int id; 
	
	private String evento; //es. codice partita (32342)
	
	private String disciplina; //es. calcio, basket..
	
	private String avvenimento; //es. milan - inter
	
	private String classe; //es. 1X2
	
	private String esito; //es. 1

	private String dataAvvenimento; //es. 11/12/2017

	private String quota; //es. 1.5

	private String timestamp; //es. 11-12-2017 16:23:11
	
	public Match() {
		super();
	}

	
	public Match(int id, String evento, String disciplina, String avvenimento, String classe, String esito,
			String dataAvvenimento, String quota, String timestamp) {
		super();
		this.id = id;
		this.evento = evento;
		this.disciplina = disciplina;
		this.avvenimento = avvenimento;
		this.classe = classe;
		this.esito = esito;
		this.dataAvvenimento = dataAvvenimento;
		this.quota = quota;
		this.timestamp = timestamp;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEvento() {
		return evento;
	}

	public void setEvento(String evento) {
		this.evento = evento;
	}

	public String getDisciplina() {
		return disciplina;
	}

	public void setDisciplina(String disciplina) {
		this.disciplina = disciplina;
	}

	public String getAvvenimento() {
		return avvenimento;
	}

	public void setAvvenimento(String avvenimento) {
		this.avvenimento = avvenimento;
	}

	public String getClasse() {
		return classe;
	}

	public void setClasse(String classe) {
		this.classe = classe;
	}

	public String getEsito() {
		return esito;
	}

	public void setEsito(String esito) {
		this.esito = esito;
	}

	public String getDataAvvenimento() {
		return dataAvvenimento;
	}

	public void setDataAvvenimento(String dataAvvenimento) {
		this.dataAvvenimento = dataAvvenimento;
	}

	public String getQuota() {
		return quota;
	}

	public void setQuota(String quota) {
		this.quota = quota;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	
	
	
	
}
