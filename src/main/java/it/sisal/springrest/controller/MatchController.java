package it.sisal.springrest.controller;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import it.sisal.springrest.bean.Match;
import it.sisal.springrest.service.MatchService;

@RestController
public class MatchController {

 MatchService matchService = new MatchService();

 @RequestMapping(value = "/matches", method = RequestMethod.GET)
 public List getMatches() {
	 List listOfMatches = matchService.getAllMatches();
	 return listOfMatches;
 }

 @RequestMapping(value = "/match/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
 public Match getMatchById(@PathVariable int id) {
	 return matchService.getMatch(id);
 }
 
 //TODO: assignment
 @RequestMapping(value = "/matches/{disciplina}", method = RequestMethod.GET, headers = "Accept=application/json")
 public List getMatchesByDisciplina(@PathVariable String disciplina) {
	 return matchService.getMatchesByDisciplina(disciplina);
 }

 @RequestMapping(value = "/match", method = RequestMethod.POST, headers = "Accept=application/json")
 public List addMatch(@RequestBody Match match) {
	 return matchService.addMatch(match);
 }
 
 
 @RequestMapping(value = "/match", method = RequestMethod.PUT, headers = "Accept=application/json")
 public Match updateMatch(@RequestBody Match match) {
	 return matchService.updateMatch(match);
 }
 
 //Serve ad aggiornare la lista di tutti i match quando arriva un messaggio da kafka
 @RequestMapping(value = "/matches", method = RequestMethod.PUT, headers = "Accept=application/json")
 public List updateMatches(@RequestBody Match match) {
	 return matchService.updateMatches(match);
 }

 @RequestMapping(value = "/match/{id}", method = RequestMethod.DELETE, headers = "Accept=application/json")
 public List deleteMatch(@PathVariable("id") int id) {
	 return matchService.deleteMatch(id);
 } 
 

}
