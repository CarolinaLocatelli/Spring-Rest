package it.sisal.springrest.service;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

import it.sisal.springrest.bean.Match;

public class MatchService {

	
	 static LinkedHashMap<Integer,Match> matchIdMap=getMatchIdMap();

	 
	 public MatchService() {
		 super();

		 if(matchIdMap==null){
			 matchIdMap=new LinkedHashMap<Integer,Match>();
			 
			 // Creating some objects of Match while initializing
			 Match match1 = new Match(0, "123", "Soccer", "Milan - Inter", "1X2", "1", "01/11/2017", "2", "01-11-2017 15:00:00");
			 Match match2 = new Match(1, "123", "Soccer", "Milan - Inter", "1X2", "X", "01/11/2017", "1,5", "01-11-2017 15:00:00");
			 Match match3 = new Match(2, "123", "Soccer", "Milan - Inter", "1X2", "2", "01/11/2017", "1,25", "01-11-2017 15:00:00");
			 Match match4 = new Match(3, "24573", "Volleyball", "Trentino Volley - Calzedonia Verona",  "1 - 2", "1", "15/11/2017", "1.27", "15-11-2017 15:00:00");
			 Match match5 = new Match(4, "24573", "Volleyball", "Trentino Volley - Calzedonia Verona",  "1 - 2",  "2", "15/11/2017","3.50", "15-11-2017 15:00:00");

			 matchIdMap.put(0, match1);
			 matchIdMap.put(1, match2);
			 matchIdMap.put(2, match3);
			 matchIdMap.put(3, match4);
			 matchIdMap.put(4, match5);
			 
		 }
	 }

	 public List getAllMatches() {
		 List matches = new ArrayList(matchIdMap.values());
		 return matches;
	 }

	 public Match getMatch(int id) {
		 Match Match= matchIdMap.get(id);
		 return Match;
	 }
	 
	 
	 //TODO: ASSIGNMENT
	 public List getMatchesByDisciplina(String disciplina){
		 List<Match> matches = new ArrayList(matchIdMap.values());
		 List<Match> result = matches.stream()
				    .filter(it -> disciplina.equals(it.getDisciplina()))
				    .collect(Collectors.toList());
		 return result;
	 }
	 
	 public List addMatch(Match match) {
		 match.setId(getMaxId()+1);
		 matchIdMap.put(match.getId(), match);
		 return getAllMatches();
	 }
	 
	 public List addMatches(List<Match> matches) {
		 for(Match m : matches){
			 matchIdMap.put(m.getId(), m);
		 }
		 return getAllMatches();
	 }
	 

	 public Match updateMatch(Match match) {
		 if(match.getId()<=0)
			 return null;
		 matchIdMap.put(match.getId(), match);
		 return match;
	 }
	 
	 public List updateMatches(Match match) {
		 //lista su cui iterare
		 List<Match> matchIdMapList = getAllMatches();

		 //lista da aggiornare per fornire il risultato
		 List<Match> list = getAllMatches();

		 //flag che diventa falso se il parametro � gi� presente nella lista oppure esso ha un valore obsoleto
		 boolean flagAggiungi = true;

		 
		 try {
			 for(Match m : matchIdMapList){
				 //se il valore � gi� presente non devo salvarlo
				 if(m.getId()==match.getId()){
					 flagAggiungi = false;
				 } else {

					 //se il valore � nuovo ed � necessario aggiornare una quota, verifico se il timestamp � pi� recente di quello gi� presente in lista
					 if(m.getEvento().equals(match.getEvento()) && m.getClasse().equals(match.getClasse()) && m.getEsito().equals(match.getEsito())){

						 SimpleDateFormat dateFormat = new SimpleDateFormat("dd-mm-yyyy hh:mm:ss");
						 Date parsedDate = dateFormat.parse(m.getTimestamp());
						 Timestamp timestampInLista = new java.sql.Timestamp(parsedDate.getTime());
						 parsedDate = dateFormat.parse(match.getTimestamp());
						 Timestamp timestampNuovo = new java.sql.Timestamp(parsedDate.getTime());

						 //rimuovo il match contenente la vecchia quota non aggiornata e aggiungo il nuovo
						 if(timestampNuovo.after(timestampInLista) && timestampNuovo!=timestampInLista && !timestampNuovo.before(timestampInLista)){
							 //metto nella lista il nuovo match al posto del vecchio
							 list.remove(m);
							 list.add(match);
						 }
						 
						 //falso perch� o ho gi� aggiunto quando ho dovuto sostituire o il messaggio era obsoleto
						 flagAggiungi=false;

					 }
				 }
				 
			 } 
			 if(flagAggiungi){
				 //aggiungo nella lista il nuovo match
				 list.add(match);
			 }

		 } catch (Exception e){
			 e.printStackTrace();
		 }

		 //aggiorno la lista su cui iterare
		 matchIdMapList = list;

		 //aggiorno la mappa che restituisce il risultato (la svuoto e aggiungo i valori della nuova lista)
		 matchIdMap.clear();

		
		 for (int i=0; i<matchIdMapList.size(); i++){
			 matchIdMap.put(i, matchIdMapList.get(i));
		 }
		 
		 //metodo che restituisce i valori presenti nella mappa matchIdMap
		 return getAllMatches();
	 }

	 
	 public List deleteMatch(int id) {
		 matchIdMap.remove(id);
		 return getAllMatches();
	 }
	 

	 public static LinkedHashMap<Integer, Match> getMatchIdMap() {
		 return matchIdMap;
	 }

	 // Utility method to get max id
	 public static int getMaxId() { 
		 int max=0;
		 for (int id:matchIdMap.keySet()) { 
			 if(max<=id)
				 max=id;
		 } 
		 return max;
	 }
}
